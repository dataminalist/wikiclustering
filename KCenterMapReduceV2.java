package it.unipd.dei.dm1617.examples; 

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.feature.IDF;
import org.apache.spark.mllib.linalg.Vector;
import scala.Tuple2;
import java.util.ArrayList;
import java.util.List;
import java.lang.Iterable; 
import java.util.Iterator; 

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Set;

import org.apache.hadoop.mapred.FileAlreadyExistsException;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.mllib.linalg.BLAS;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.feature.Word2Vec;
import org.apache.spark.mllib.feature.Word2VecModel;

import it.unipd.dei.dm1617.CountVectorizer;
import it.unipd.dei.dm1617.Distance;
import it.unipd.dei.dm1617.InputOutput;
import it.unipd.dei.dm1617.Lemmatizer;
import it.unipd.dei.dm1617.WikiPage;
import scala.Tuple2;
//import scala.collection.immutable.List;



/**
 * Algoritmo di Map-Reduce basato su key-center 
 *
 *@ authors A.Ciresola, F.Paganin 
 *@ version 2.1 
 *@ Aggiunta la parte di preprocessing in cui scarto i documenti con un numero troppo piccolo di parole 
 *@ date 07/05/2017 
 *@ last review 08/05/2017 
 */

/********************************************************************************************************
----------------------------> COMPILA MA NON ANCORA ESEGUIBILE <---------------------------------------
********************************************************************************************************/


public class KCenterMapReduceV2 {

  public static void main(String[] args)

   {
      // String dataPath = args[0];
       String dataPath = ("medium-sample.dat.bz2"); 
    
 
    // Usual setup
    SparkConf conf = new SparkConf(true).setAppName("k-Center_Map_Reduce");
    JavaSparkContext sc = new JavaSparkContext(conf);

    // Load dataset of pages
    JavaRDD<WikiPage> pages = InputOutput.read(sc, dataPath); 

    // Get text out of pages
    JavaRDD<String> texts = pages.map((p) -> p.getText());

    // Get the lemmas. It's better to cache this RDD since the
    // following operation, lemmatization, will go through it two
    // times.
    JavaRDD<ArrayList<String>> lemmas = Lemmatizer.lemmatize(texts).cache();

    // Transform the sequence of lemmas in vectors of counts in a
    // space of 100 dimensions, using the 100 top lemmas as the vocabulary.
    // This invocation follows a common pattern used in Spark components:
    //
    //  - Build an instance of a configurable object, in this case CountVectorizer.
    //  - Set the parameters of the algorithm implemented by the object
    //  - Invoke the `transform` method on the configured object, yielding
    //  - the transformed dataset.
    //
    // In this case we also cache the dataset because the next step,
    // IDF, will perform two passes over it.

    JavaRDD<Vector> tf = new CountVectorizer()
      .setVocabularySize(100)
      .transform(lemmas)
      .cache();

    // Same as above, here we follow the same pattern, with a small
    // addition. Some of these "configurable" objects configure their
    // internal state by means of an invocation of their `fit` method
    // on a dataset. In this case, the Inverse Document Frequence
    // algorithm needs to know about the term frequencies across the
    // entire input dataset before rescaling the counts of the single
    // vectors, and this is what happens inside the `fit` method invocation.

    JavaRDD<Vector> tfidf = new IDF()
      .fit(tf)
      .transform(tf);

    // In this last step we "zip" toghether the original pages and
    // their corresponding tfidf vectors. We can perform this
    // operation safely because we did no operation changing the order
    // of pages and vectors within their respective datasets,
    // therefore the first vector corresponds to the first page and so
    // on.
    
    // pages = JavaRDD<WikiPage> ; 
    // tfidf = JavaRDD<Vector> ;  


    JavaPairRDD<WikiPage, Vector> pagesAndVectors = pages.zip(tfidf);

    // the pointset P = pagesAndVectors 
    //@param n number of elements in the pointset 
    int n = (int)pagesAndVectors.count();  

    //@param l number of subsets of pagesAndVectors 
    double l = Math.sqrt(n); 
    //@param appl int number of subsets 
    int appl = (int)Math.floor(l); 

    //map of each element <Wi, Vi> into < <Wi, Vi> , i > ; 
    // first element gets index 0, secondo element gets index 1 and so on... 
    JavaPairRDD< Tuple2<WikiPage,Vector> , Long > pagesAndVectorsWithIndex = pagesAndVectors.zipWithIndex(); 

    //new indexes assigned at each pair with index that points to the bucket index from 0 to sqrt(n)-1
    JavaPairRDD< Integer , Tuple2<WikiPage,Vector> > newpagesAndVectors = pagesAndVectorsWithIndex.mapToPair(
        
        (tuple) -> 
         
        { 
            int tmp = (int) (long) (tuple._2() ); 
            int index = (tmp%appl); 
 
            return new Tuple2< Integer , Tuple2<WikiPage, Vector> >(index , tuple._1() ); 
        }

        ); 


     //each element of the following RDD is a subset Pj of the initiali pointset P 
     JavaPairRDD< Integer, Iterable<Tuple2<WikiPage, Vector> > > pagesGroupedByKey = newpagesAndVectors.groupByKey(); 
     
     //at this point we have to run the Farthest-First Traversal algorithm on each element of the previous RDD 
    // Now we want to do this:  JavaPairRDD<Integer, Iterable<Tuple2<Wikipage, Vector>>> converted to 
    //   JavaPairRDD <Integer, ArrayList<Tuple2<WikiPage, Vector>>> , because this last one is more simple to manage 
    
    JavaPairRDD< Integer, ArrayList< Tuple2<WikiPage, Vector> > > pagesGroupedByKeyArrayList = pagesGroupedByKey.mapToPair( 
          
          (tuple) -> 
          { 
            ArrayList< Tuple2<WikiPage, Vector> > tempArray = new ArrayList< Tuple2<WikiPage, Vector> >(); 
            Iterator<Tuple2<WikiPage, Vector>> newIterator = tuple._2().iterator(); 
            
            while(newIterator.hasNext()) 
            {
                tempArray.add(newIterator.next()); 
            }
            
            return new Tuple2<Integer, ArrayList<Tuple2<WikiPage, Vector>>>(tuple._1() , tempArray); 

          }

        );

    // Now using the Farthest-First Traversal algorithm we need to calculate the set of centers for each element 
    //in the pagesGroupedByKeyArrayList JavaPairRDD 

    JavaPairRDD< Integer, ArrayList< Tuple2<WikiPage, Vector> > > centersForEachSubset = pagesGroupedByKeyArrayList.mapToPair(

        (tuple) -> 
         { 
             return farthestFirstTraversal(tuple); 
         }
        );
     
    //***********end Round 1*************** 



    // Arrivati a questo punto abbiamo terminato il Round 1 e centersForEachSubset contiene tutti gli indici dei sottoinsiemi 
    // con i centri selezionati per tali sottoinsiemi 


    //**************Round 2****************
        
    JavaPairRDD< Integer, ArrayList< Tuple2<WikiPage, Vector> > > tuplesToJoin = centersForEachSubset.mapToPair( (tuple) -> 
    {
        return new Tuple2< Integer, ArrayList< Tuple2<WikiPage, Vector>> >( 0, tuple._2() ); 
    } );


    JavaPairRDD< Integer, ArrayList< Tuple2<WikiPage, Vector> > > tuplesToJoin2 = tuplesToJoin.reduceByKey( 
        (value1, value2) -> 
        {
           //eseguo l'unione dei due arraylist 
            value2.forEach( (elem) -> 
            {
                value1.add(elem); 
            });

            return value1; 
        });

    // A questo punto tuplesToJoin2 contiene una sola tupla del tipo (k,V) con K=0 e value = unione di tutti i centri 
    // Determiniamo ora i k-centri a partire dall'insieme di k*sqrt(n) centri applicando nuovamente l'algoritmo di Farthest_First

    // Estraggo l'unica tupla presente nel precedente RDD 
    Tuple2< Integer, ArrayList< Tuple2<WikiPage, Vector> > > tuplesToJoin3 = tuplesToJoin2.first(); 
    
    // Applico il Farthest-First Traversal per il calcolo dei k centri definitivi 
    Tuple2< Integer, ArrayList< Tuple2<WikiPage, Vector> > > finalCenters = farthestFirstTraversal(tuplesToJoin3); 

    ArrayList< Tuple2<WikiPage, Vector> > extractedCenters = finalCenters._2(); 

    //Stampa diagnostica
    extractedCenters.forEach((elem) -> 
        {
            System.out.println(elem._1().getTitle()); 
        });

//*******************end Round 2**********************************

//*********************Round 3***********************************



  }//[m]end main  


// l'algoritmo deve restituire una tupla, per ciascun documento devo calcolare k centri 
// @input = subset Pj 
// @output = Tuple2< Integer, ArrayList< Tuple2<WikiPage, Vector> > > 

public static Tuple2< Integer, ArrayList< Tuple2<WikiPage, Vector> > > farthestFirstTraversal(
    Tuple2< Integer, ArrayList< Tuple2<WikiPage, Vector> > > inputSubset ) 
{
        //inputSubset = subset of elements for which we have to calculate centers 

        // K-center clustering 
        Integer subsetID = inputSubset._1(); 

        //subsetElements contiene i punti del subset Pj 
        ArrayList< Tuple2<WikiPage, Vector> > subsetElements = inputSubset._2(); 

        // number of points of the subset Pj that I consider 
        int numberOfTuples = subsetElements.size(); 

        //ArrayList in cui salvo i centri 
        ArrayList< Tuple2<WikiPage,Vector> > centers = new ArrayList< Tuple2<WikiPage,Vector> >();
         
        // number of clusters for each subset Pj 
        final int K = 50;
        
        // use first element as first center
        // Vector --> riferito ad una pagina di Wikipedia 
        // ArrayList<String> = insieme di tutte le stringhe che costituiscono il testo di una pagina di Wikipedia 
        
        int minWordsNumber = 30;
        boolean flag = true; 

        //Punto di partenza 
        while(flag)
        {
          int index = randVal(0, numberOfTuples-1); 
          Tuple2<WikiPage,Vector> center = subsetElements.get(index); 
          WikiPage newPage = center._1(); 
          String text = newPage.getText(); 
          String[] words = text.split(" "); 
          int numOfWords = words.length; 
        
          if(numOfWords > minWordsNumber)
          { 
             //aggiungo il punto a centers 
             centers.add(center); 
             //rimuovo il centro da subsetElements 
             subsetElements.remove(index);
             flag = false; 
          }
        
        }
        

        while(centers.size() != K) 
        {   
            
            ArrayList<Tuple2<Tuple2<WikiPage, Vector>, Double >> finalDistances = 
            new ArrayList<Tuple2<Tuple2<WikiPage, Vector>, Double >>(); 

            subsetElements.forEach( (point) ->
            {   
                WikiPage page = point._1(); 
                String text = page.getText(); 
                String[] words = text.split(" "); 
                int numOfWords = words.length;

                // solo se il punto preso rispetta i vincoli sul numero minimo di parole calcolo le distanze 
                // di tale punto dai centri, prendendo poi il minimo di tali distanze; 
                // per tutti quei punti che non rispettano il numero minimo di parole è inutile calcolarne le distanze 
                // dai centri in quanto si sprecherebbe solo tempo! 

                if(numOfWords > minWordsNumber)
                { 
                    ArrayList<Double> tempDistances = new ArrayList<Double>();

                    centers.forEach( (cent) -> 
                    {    
                        double dist = Distance.cosineDistance(point._2(), cent._2() ); 
                        tempDistances.add(dist); 

                    });

                    //prendo il minimo di queste distanze 
                    double elem = tempDistances.get(0); 
                    double min = elem; 
                    for(int h=1; h<tempDistances.size(); h++)
                    {
                        if(tempDistances.get(h) < min)
                          min = tempDistances.get(h); 
                    }

                    finalDistances.add(new Tuple2<Tuple2<WikiPage, Vector>, Double >(point, min)); 

                   
                }
                    
            } ); 

                //prendo il massimo delle distanze contenute in finalDistances 
                Tuple2< Tuple2<WikiPage, Vector> , Double > newTuple = finalDistances.get(0); 
                Tuple2< Tuple2<WikiPage, Vector> , Double > max = newTuple; 

                for(int r=1; r<finalDistances.size(); r++)
                {
                    if(finalDistances.get(r)._2() > max._2())
                    {
                        max = finalDistances.get(r); 
                    }
                }


                // max contiene ora il centro desiderato 
                // aggiungo max a centers e rimuovo max da subsetElements 
                centers.add(max._1()); 
                subsetElements.remove(max._1()); 
            
        } 


return new Tuple2< Integer, ArrayList< Tuple2<WikiPage, Vector> > >(subsetID, centers); 
   

}//end[m]farthest_First_Traversal 



//restituisce un intero compreso tra minVal e maxVal 
public static int randVal(int minVal, int maxVal)
    { 
        int diff = maxVal-minVal;
        return (int)(Math.random()*((double)diff+1.0d))+minVal;
    }



}//[c]end class 
