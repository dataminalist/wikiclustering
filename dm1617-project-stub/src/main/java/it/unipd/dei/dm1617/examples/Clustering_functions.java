package it.unipd.dei.dm1617.examples;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.mllib.feature.Word2VecModel;
import org.apache.spark.mllib.linalg.BLAS;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;

import it.unipd.dei.dm1617.Distance;
import it.unipd.dei.dm1617.WikiPage;
import scala.Tuple2;

public class Clustering_functions {

	  /* 
	   * @brief distribute the model in broadcast to the workers and calculate for each document the corresponding vector
	   * @author D.Lucchi
	   * @version 0.1
	   * 
	   * @param JavaSparkContext sc, Word2VecModel model, JavaPairRDD<WikiPage, ArrayList<String>> documents, int dim
	   * @returns JavaRDD<Tuple2<WikiPage,Vector>>
	  */
	  static JavaPairRDD<WikiPage,Vector> documentToVector(JavaSparkContext sc,
			  Word2VecModel model,
			  JavaPairRDD<WikiPage, ArrayList<String>> documents, int dim) {
		  
			Broadcast<Word2VecModel> bStopWords = sc.broadcast(model);
			
			return documents.mapToPair((doc) -> {
				
				Word2VecModel sw = bStopWords.getValue();
				Iterator<String> it = doc._2().iterator();
				Vector doc_v = Vectors.zeros(dim);
				
				Vector wordVec;
				int count = 0;

				while (it.hasNext())
				{
					try
					{
						wordVec = sw.transform((String)it.next());
						BLAS.axpy(1, wordVec, doc_v);
					}
					
					/* When a word is not in the vocabulary because it is infrequent, the transform method throws an ecception.
					 * However it is not necessary to compute also the infrequent words.
					 */
					catch (Exception e)
					{
						count--;
					}
					
					count++;
				}
				BLAS.scal(count, doc_v);
				
				return new Tuple2<WikiPage,Vector>(doc._1(),doc_v);
			});
		}
	  
	 
	  /*
	   *  @brief Assign documents to their closest center
	   *  @author D.Lucchi
	   *  @version 0.1
	   *  
	   *  @param JavaSparkContext sc, ArrayList<Vector> centers, JavaRDD<Tuple2<WikiPage,Vector>> docVec
	   *  @returns JavaRDD<Tuple2<Tuple2<WikiPage,Vector>,Integer>>: <Document, cluster number>
	   */
	   
	  static JavaRDD<Tuple2<Tuple2<WikiPage,Vector>,Integer>> partition(
			  JavaSparkContext sc,
			  ArrayList<Vector> centers,
			  JavaRDD<Tuple2<WikiPage,Vector>> docVec)
	  {
		  
		  return docVec.map((doc) -> {
			  
			  double dist = 2;
			  int cluster = 0;
			  for (int i = 0, n = centers.size(); i < n; i++)
			  {
				  double curDist = Distance.cosineDistance(centers.get(i), doc._2());
				  if (curDist < dist)
				  {
					  dist = curDist;
					  cluster = i;
				  }
			  }
			  
			  return new Tuple2<Tuple2<WikiPage,Vector>,Integer>(doc,cluster);
		  });
		  
	  }
	  
	  /*
	   * @brief Function to calculate the objective function
	   * 
	   * @author T.Agnolazza
	   * @version 0.0
	   * 
	   * @param JavaPairRDD< Integer , Iterable<Tuple2<WikiPage, Vector>> > cluster, ArrayList< Tuple2 < Integer, Tuple2<WikiPage, Vector>>> centres
	   * @returns Double obj_funct
	   */
	  static Double objectiveFunction (JavaPairRDD< Integer , Iterable<Tuple2<WikiPage, Vector>>> cluster,
			  ArrayList< Tuple2 < Integer, Tuple2<WikiPage, Vector>>> centres,
			  JavaSparkContext sc){
		
		  Broadcast<ArrayList< Tuple2 < Integer, Tuple2<WikiPage, Vector>>>> arrayCentre = sc.broadcast(centres);
		  
		  /*
		   *Computing maximum distances inside clusters 
		   */
		  JavaRDD<Tuple2<Integer, Double>> distances = cluster.map((elem)->{
			  double maxDistCluster = 0;
			  int clust = elem._1;
			  Vector currentCentre = null;
			  
			  //Looking for the corresponding centre
			  for(Tuple2 < Integer, Tuple2<WikiPage, Vector>> t : arrayCentre.value()){
				  if (clust == t._1){
					  currentCentre = t._2._2;
					  break;
				  }  
			  }
			  
			  //Calculating distances with current centre
			  for(Tuple2<WikiPage, Vector> t : elem._2){
				  double tmp = Distance.cosineDistance(currentCentre, t._2);
				  if (tmp > maxDistCluster)
					  maxDistCluster = tmp;
			  }
			  return new Tuple2<Integer, Double>(clust, maxDistCluster);
		  });
		  
		  /*
		   * To see max distances in clusters
		   */
		  for (Tuple2<Integer, Double> t : distances.collect()){
		  	  System.out.println("\nPartial cluster: " + t._1 + " max dist " + t._2.toString());
		  }
		  
		  return distances.reduce((elem1, elem2)->{
			  if (elem1._2.compareTo(elem2._2) > 0)
				  return elem1;
			  return elem2;
		  })._2;
		  
	  }
	  
	  /*
	   * @brief Class that allow to extract the most frequent classes in the document.
	   * 
	   * @author T.Agnolazza
	   * @version 0.1
	   * 
	   * @param JavaPairRDD<Integer , Tuple2<WikiPage, Vector>> documents classified into clusters, 
	   * 			int minimum number of repetition of the same category in the hold document set, 
	   * 			int minimum percentage of the cateogry inside a cluster
	   */
	  public static void classFrequency (JavaPairRDD<Integer , Tuple2<WikiPage, Vector>> clusters, int totalThreshold, int relativeThreshold){
		  
		  JavaPairRDD<Tuple2<String, Integer> , Integer> classes = clusters.flatMapToPair((elem)->{
			  
			  String cat[] = elem._2._1.getCategories();
			  ArrayList<Tuple2<Tuple2<String, Integer> , Integer>> toReturn = new  ArrayList<Tuple2<Tuple2<String, Integer> , Integer>>();
			  for (String e : cat){
				  toReturn.add(new Tuple2<Tuple2<String, Integer> , Integer>(new Tuple2<String, Integer>(e,elem._1), 1));
			  }
			  return toReturn.iterator();
		  });
		  
		  //JavaPairRDD<Tuple2<Category, Cluster> , Partial>
		  JavaPairRDD<Tuple2<String, Integer> , Integer> partFreq = classes.reduceByKey((elem1,elem2)->{
			  return elem1+elem2;
		  });
		  
		  //JavaPairRDD<Tuple2<Category, Cluster> , Tuple2<Parziale,Totale>>
		  JavaPairRDD<Tuple2<String, Integer> , Tuple2<Integer,Integer>> totalFreq = partFreq.mapToPair((elem)->{
			  //tmp ArrayList<Tuple2<Cluster, Parziale>>
			  ArrayList<Tuple2<Integer, Integer>> tmp = new ArrayList<Tuple2<Integer, Integer>>();
			  tmp.add(new Tuple2<Integer,Integer>(elem._1._2,elem._2));
			  Tuple2<Integer, ArrayList<Tuple2<Integer, Integer>>> tmp2 = new Tuple2<Integer, ArrayList<Tuple2<Integer, Integer>>>(elem._2,tmp);
			  return new Tuple2<String, Tuple2<Integer, ArrayList<Tuple2<Integer, Integer>>>>(elem._1._1, tmp2);
		  }).reduceByKey((elem1,elem2)->{
			  elem1._2.add(elem2._2.get(0));
			  return new Tuple2<Integer, ArrayList<Tuple2<Integer, Integer>>>(elem1._1+elem2._1, elem1._2);
		  }).flatMapToPair((elem)->{
			  ArrayList<Tuple2<Tuple2<String, Integer> , Tuple2<Integer,Integer>>> tmp = new ArrayList<Tuple2<Tuple2<String, Integer> , Tuple2<Integer,Integer>>>();
			  
			  for(Tuple2<Integer, Integer> e : elem._2._2){
				  tmp.add(new Tuple2<Tuple2<String, Integer> , Tuple2<Integer,Integer>>(new Tuple2<String, Integer>(elem._1,e._1),new Tuple2<Integer,Integer>(e._2,elem._2._1)));
			  }
			  return tmp.iterator();
		  });
		  
		  /*JavaPairRDD<Integer, Iterable<Tuple2<String, Integer>>> reshuffle = partFreq.mapToPair((elem)->{
			  return new Tuple2<Integer, Tuple2<String, Integer>>(elem._1._2, new Tuple2<String, Integer>(elem._1._1,elem._2));
		  }).filter((elem)->{
			  return elem._2._2 > threshold;
		  }).groupByKey().sortByKey();
		  */
		  
		  JavaPairRDD<Integer, Iterable<Tuple2<String, Tuple2<Integer, Integer>>>> reshuffle1 = totalFreq.mapToPair((elem)->{
			  Tuple2<String, Tuple2<Integer, Integer>> tmp = new Tuple2<String, Tuple2<Integer, Integer>>(elem._1._1,elem._2);
			  return new Tuple2<Integer, Tuple2<String, Tuple2<Integer, Integer>>>(elem._1._2, tmp);
		  }).filter((elem)->{
			  return Double.compare(((double)elem._2._2._1/elem._2._2._2*100), relativeThreshold) >= 0;
		  }).filter((elem)->{
			  return elem._2._2._2 >= totalThreshold;
		  }).groupByKey().sortByKey();
		  		  
		  
		  for(Tuple2<Integer, Iterable<Tuple2<String, Tuple2<Integer, Integer>>>> elem : reshuffle1.collect()){
			  System.out.println("Cluster: "+ elem._1);
			  for(Tuple2<String, Tuple2<Integer, Integer>> e: elem._2){
				  System.out.println("Class: "+e._1+" for "+ Math.round((double)e._2._1/e._2._2*100)+ " relative frequency.");
			  }
		  }
		  
	  }
}
