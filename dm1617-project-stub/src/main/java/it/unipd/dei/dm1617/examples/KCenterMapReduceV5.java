package it.unipd.dei.dm1617.examples; 

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.feature.IDF;
import org.apache.spark.mllib.linalg.Vector;
import scala.Tuple2;
import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.Iterable; 
import java.util.Iterator; 
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Set;
import org.apache.hadoop.mapred.FileAlreadyExistsException;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.mllib.linalg.BLAS;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.feature.Word2Vec;
import org.apache.spark.mllib.feature.Word2VecModel;
import it.unipd.dei.dm1617.CountVectorizer;
import it.unipd.dei.dm1617.Distance;
import it.unipd.dei.dm1617.InputOutput;
import it.unipd.dei.dm1617.Lemmatizer;
import it.unipd.dei.dm1617.WikiPage;
import scala.Tuple2;
import java.util.concurrent.TimeUnit;
import it.unipd.dei.dm1617.examples.Clustering_functions;


/**
 * Algoritmo di Map-Reduce basato su key-center 
 *
 *@ authors A.Ciresola, F.Paganin 
 *@ version 5.0 
 *@ Round 3 ottimizzato 
 *@ Non calcolo più Partition(P,S), ma Partition(Pj, S) per ogni sottoinsieme Pj in parallelo  
 *@ date 14/05/2017 
 *@ last review 14/05/2017 
 */


public class KCenterMapReduceV5 
{

  public static void main(String[] args) throws IOException
  {
	// start monitoring for the TRAINING execution time
	long startTrainingTime = System.currentTimeMillis();
			  
	// String dataPath = args[0];
	String dataPath = ("small-sample.dat.bz2"); 
	//int k = Integer.parseInt("100");//args[0]); //<---------- K viene fornito come parametro in input

	
    // Usual setup
    SparkConf conf = new SparkConf(true).setAppName("k-Center_Map_Reduce");
    JavaSparkContext sc = new JavaSparkContext(conf);

    // Load dataset of pages
    JavaRDD<WikiPage> pages = InputOutput.read(sc, dataPath); 

    // Get text out of pages
    JavaRDD<String> texts = pages.map((p) -> p.getText());

    // Get the lemmas. It's better to cache this RDD since the
    // following operation, lemmatization, will go through it two
    // times.
    JavaRDD<ArrayList<String>> lemmas = Lemmatizer.lemmatize(texts).cache();
    
    //vector space dimension
    final int dim = 100;
    
    // fit the word2vec model and save in memory
    // run only once and then comment out
    // CHANGE THE PARAMETER OF FIT TO documents OR lemmas ACCORDINGLY TO THE CHOSEN PREPROCESSING
    /*new Word2Vec()
    		.setVectorSize(dim)
    		.fit(lemmas).save(sc.sc(), "Models/");*/
    
    // stop monitoring for the TRAINING execution time
    long stopTrainingTime = System.currentTimeMillis();
    long elapsedTrainingTime = stopTrainingTime - startTrainingTime;
    System.out.println("Training executed in " + TimeUnit.MILLISECONDS.toSeconds(elapsedTrainingTime) + " seconds");
    
    // start monitoring for the DOCTOVEC execution time
    long startDocToVecTime = System.currentTimeMillis();
    
    // load model from memory
    Word2VecModel model = Word2VecModel.load(sc.sc(), "Models/");
   
    JavaPairRDD<WikiPage, ArrayList<String>> pagesAndLemma = pages.zip(lemmas);
    
    // map documents to vectors
    JavaPairRDD<WikiPage,Vector> pagesAndVectors =  Clustering_functions.documentToVector(sc, model, pagesAndLemma, dim);
    
    // stop monitoring for the DOCTOVEC execution time
    long stopDocToVecTime = System.currentTimeMillis();
    long elapsedDocToVecTime = stopDocToVecTime - startDocToVecTime;
    
    /*
    // Transform the sequence of lemmas in vectors of counts in a
    // space of 100 dimensions, using the 100 top lemmas as the vocabulary.
    // This invocation follows a common pattern used in Spark components:
    //
    //  - Build an instance of a configurable object, in this case CountVectorizer.
    //  - Set the parameters of the algorithm implemented by the object
    //  - Invoke the `transform` method on the configured object, yielding
    //  - the transformed dataset.
    //
    // In this case we also cache the dataset because the next step,
    // IDF, will perform two passes over it.

    JavaRDD<Vector> tf = new CountVectorizer()
      .setVocabularySize(100)
      .transform(lemmas)
      .cache();

    // Same as above, here we follow the same pattern, with a small
    // addition. Some of these "configurable" objects configure their
    // internal state by means of an invocation of their `fit` method
    // on a dataset. In this case, the Inverse Document Frequence
    // algorithm needs to know about the term frequencies across the
    // entire input dataset before rescaling the counts of the single
    // vectors, and this is what happens inside the `fit` method invocation.

    JavaRDD<Vector> tfidf = new IDF()
      .fit(tf)
      .transform(tf);

    // In this last step we "zip" toghether the original pages and
    // their corresponding tfidf vectors. We can perform this
    // operation safely because we did no operation changing the order
    // of pages and vectors within their respective datasets,
    // therefore the first vector corresponds to the first page and so
    // on.
    
    // pages = JavaRDD<WikiPage> ; 
    // tfidf = JavaRDD<Vector> ;  


    JavaPairRDD<WikiPage, Vector> pagesAndVectors = pages.zip(tfidf).cache();

	*/
    
    // Open a file where all the gathered information is going to be written to
    PrintWriter output = new PrintWriter(new File("Analysis results.csv"));
    // the output file is a csv with the following columns
    output.println("Iteration,k,k_coeff,k_first,R1_time,R2_time,R3_time,f_obj,notes,");
    output.flush();
    int iteration_count = 0;
    
    try
    {
    // the values of k are: 10, 15, 23, 34, 51, 77, 115, 172, 258, 387
    for (int k = 10; k < 400; k = (int)(1.5d*k))
    {
    	for (double k_coeff = 1; k_coeff <= 3; k_coeff+=0.5)
    	{
    		
    	
		    //******************************************Round 1****************************************************************
		    
		    // monitoring for the ROUND1 execution time 
		    long startRoundOneTime = System.currentTimeMillis();
		    
		    // the pointset P = pagesAndVectors 
		    //@param n number of elements in the pointset 
		    int n = (int)pagesAndVectors.count();  
		
		    //@param l number of subsets of pagesAndVectors 
		    double l = Math.sqrt(n/k); 
		    //@param appl int number of subsets 
		    int appl = (int)Math.floor(l); 
		
		    //map of each element <Wi, Vi> into < <Wi, Vi> , i > ; 
		    // first element gets index 0, secondo element gets index 1 and so on... 
		    JavaPairRDD< Tuple2<WikiPage,Vector> , Long > pagesAndVectorsWithIndex = pagesAndVectors.zipWithIndex(); 
		
		    //new indexes assigned at each pair with index that points to the bucket index from 0 to sqrt(n/k)-1
		    JavaPairRDD< Integer , Tuple2<WikiPage,Vector> > newpagesAndVectors = pagesAndVectorsWithIndex.mapToPair(
		        
		        (tuple) -> 
		         
		        { 
		            int tmp = (int) (long) (tuple._2() ); 
		            int index = (tmp%appl); 
		 
		            return new Tuple2< Integer , Tuple2<WikiPage, Vector> >(index , tuple._1() ); 
		        }
		
		        ); 
		
		
		     //each element of the following RDD is a subset Pj of the initiali pointset P 
		     JavaPairRDD< Integer, Iterable<Tuple2<WikiPage, Vector> > > pagesGroupedByKey = newpagesAndVectors.groupByKey(); 
		     
		     //at this point we have to run the Farthest-First Traversal algorithm on each element of the previous RDD 
		    // Now we want to do this:  JavaPairRDD<Integer, Iterable<Tuple2<Wikipage, Vector>>> converted to 
		    //   JavaPairRDD <Integer, ArrayList<Tuple2<WikiPage, Vector>>> , because this last one is more simple to manage 
		    
		    JavaPairRDD< Integer, ArrayList< Tuple2<WikiPage, Vector> > > pagesGroupedByKeyArrayList = pagesGroupedByKey.mapToPair( 
		          
		          (tuple) -> 
		          { 
		            ArrayList< Tuple2<WikiPage, Vector> > tempArray = new ArrayList< Tuple2<WikiPage, Vector> >(); 
		            Iterator<Tuple2<WikiPage, Vector>> newIterator = tuple._2().iterator(); 
		            
		            while(newIterator.hasNext()) 
		            {
		                tempArray.add(newIterator.next()); 
		            }
		            
		            return new Tuple2<Integer, ArrayList<Tuple2<WikiPage, Vector>>>(tuple._1() , tempArray); 
		
		          }
		
		        );
		
		    // Now using the Farthest-First Traversal algorithm we need to calculate the set of centers for each element 
		    //in the pagesGroupedByKeyArrayList JavaPairRDD 
		    // We need to check if the value of k' is compatible with the number of elements inside each subsets.
		    int elements_per_subset = n/appl;
		    int k_first = (int)(k_coeff*k);
		    if (k_first > elements_per_subset)
		    {
		    	// we must stop 
		    	// we were trying to take from each subset a number of centers greater than the number of elements in the subset
		    	// format: Iteration,k,k_coeff,k_first,R1_time,R2_time,R3_time,f_obj,notes
		    	output.println(iteration_count+","+k+","+k_coeff+","+k_first+",,,,,K_first "+k_first+ " (k = "+k+", k_coeff = "+k_coeff+") is bigger than the number of elements for each subset "+elements_per_subset);
		    	// Force the writing to the output file so if things go wrong we know everything up to the last iteration
		    	output.flush();
		    	iteration_count++;
		    	continue;
		    }
		
		    JavaPairRDD< Integer, ArrayList< Tuple2<WikiPage, Vector> > > centersForEachSubset = pagesGroupedByKeyArrayList.mapToPair(
		
		        (tuple) -> 
		         {   
		             // Invoco il Farthest-First Traversal con un parametro k' > k, in questo caso specifico k'= 2k 
		             return farthestFirstTraversal(tuple ,k_first); 
		         }
		        ).cache(); 
		    
		    // stop monitoring for the ROUND1 execution time
		    long stopRoundOneTime = System.currentTimeMillis();
		    long elapsedRoundOneTime = stopRoundOneTime - startRoundOneTime;
		    //***********end Round 1*************** 
		
		
		
		    // Arrivati a questo punto abbiamo terminato il Round 1 e centersForEachSubset contiene tutti gli indici dei sottoinsiemi 
		    // con i centri selezionati per tali sottoinsiemi 
		
		
		    //**************Round 2****************
		    
		    // monitoring for the ROUND2 execution time 
		    long startRoundTwoTime = System.currentTimeMillis();
		        
		    JavaPairRDD< Integer, ArrayList< Tuple2<WikiPage, Vector> > > tuplesToJoin = centersForEachSubset.mapToPair( (tuple) -> 
		    {
		        return new Tuple2< Integer, ArrayList< Tuple2<WikiPage, Vector>> >( 0, tuple._2() ); 
		    } );
		
		
		    JavaPairRDD< Integer, ArrayList< Tuple2<WikiPage, Vector> > > tuplesToJoin2 = tuplesToJoin.reduceByKey( 
		        (value1, value2) -> 
		        {
		           //eseguo l'unione dei due arraylist 
		            value2.forEach( (elem) -> 
		            {
		                value1.add(elem); 
		            });
		
		            return value1; 
		        }).cache();
		
		    // A questo punto tuplesToJoin2 contiene una sola tupla del tipo (k,V) con K=0 e value = unione di tutti i centri 
		    // Determiniamo ora i k-centri a partire dall'insieme di k*sqrt(n) centri applicando nuovamente l'algoritmo di Farthest_First
		
		    // Estraggo l'unica tupla presente nel precedente RDD 
		    Tuple2< Integer, ArrayList< Tuple2<WikiPage, Vector> > > tuplesToJoin3 = tuplesToJoin2.first(); 
		    
		    // Applico il Farthest-First Traversal per il calcolo dei k centri definitivi 
		    Tuple2< Integer, ArrayList< Tuple2<WikiPage, Vector> > > finalCenters = farthestFirstTraversal(tuplesToJoin3, k); 
		
		    // extractedCenters = S = {c1, c2, ..., ck} = k centri definitivi 
		    ArrayList< Tuple2<WikiPage, Vector> > extractedCenters = finalCenters._2(); 
		
		   
		        //Stampa diagnostica
		        extractedCenters.forEach((elem) -> 
		         {   
		             String[] temp = elem._1().getCategories(); 
		             String temp1 = ""; 
		             for(int a = 0; a<temp.length; a++)
		             {
		                temp1 = ( temp1 + ", " + temp[a] ); 
		             }
		             System.out.println("Pagina di Wikipedia numero:[" + elem._1().getId()+"], [Titolo:"+ elem._1().getTitle()+ 
		                "], Categorie:[" + temp1 + "]"); 
		         });
		        
		     // stop monitoring for the ROUND2 execution time
		     long stopRoundTwoTime = System.currentTimeMillis();
		     long elapsedRoundTwoTime = stopRoundTwoTime - startRoundTwoTime;  
		   
			//********************end Round 2*********************************
			
			//*********************Round 3************************************
			
			// extractedCenters = S sono i k centri estratti necessari per Partition(P,S)
			// pagesAndVectors = P insieme completo di <WikiPage, Vector> 
			
			// cosineDistance(Vector_1, Vector_2)==0 <--> Vector_1==Vector_2 
			// Per ogni elemento presente in pagesAndVectors calcolo la cosineDistance di ciascun elemento dai centri presenti in 
			// extractedCenters, se la distanza è zero significa che il punto preso è un centro non lo inserisco nel cluster inn quanto 
			        //già presente 
			
			// Clusters --> li considero come Tuple2< Integer, ArrayList<WikiPage, Vector>> (Indice_cluster, Punti del cluster)
			// Per ogni centro in extractedCenters creo un cluster contenente solo quel centro 
			
			// Per ogni punto in pagesAndVectors calcolo la distanza da tutti i centri e prendo la minima, assegnando 
			// tale punto al cluster corrispondente 
			
			//********************************************************************************************************************************
		    
		    // monitoring for the ROUND2 execution time 
		    long startRoundThreeTime = System.currentTimeMillis(); 
		    
			ArrayList< Tuple2 < Integer, Tuple2<WikiPage, Vector > > > clusters = new ArrayList< Tuple2<Integer, Tuple2<WikiPage, Vector >>>(); 
			
			for(int j=0; j<extractedCenters.size(); j++) 
			{
			  clusters.add(new Tuple2<Integer, Tuple2<WikiPage, Vector >>(Integer.valueOf(j) , extractedCenters.get(j)) );   
			}
			
			
			// clusters contiene tutte le coppie (0, c0), (1,c1),...,(k-1, c(k-1))  
			// dove la chiave è l'indice del cluster mentre il valore è il centro del cluster corrispondente
			
			// Insieme di tutti i sottoinsiemi Pj = pagesGroupedByKeyArrayList 
			// JavaPairRDD< Integer, ArrayList< Tuple2<WikiPage, Vector> > > pagesGroupedByKeyArrayList 
			
			// Insieme S dei k centri finali con indici = clusters
			// ArrayList< Tuple2 < Integer, Tuple2<WikiPage, Vector > > > clusters 
			
			
			// Numero di ArrayList pari al numero di sottoinsiemi Pj = appl= sqrt(n/k) 
			// Integer --> indice del cluster in cui deve andare l'elemento 
			// Tuple2 --> elemento 
			
			JavaRDD< ArrayList< Tuple2< Integer , Tuple2<WikiPage, Vector> >> > clustersRound3 = pagesGroupedByKeyArrayList.map(
			      
			      (tuple) -> 
				  {
				     return partition(tuple, clusters); 
				
				  }
			  ); 
			
			
			// funzione di map fittizia, necessaria per il successivo reduceByKey 
			JavaPairRDD<Integer,  ArrayList< Tuple2< Integer , Tuple2<WikiPage, Vector> >> > fix = clustersRound3.mapToPair((tuple) ->
			  { 
			   
			    return new Tuple2<Integer, ArrayList< Tuple2< Integer , Tuple2<WikiPage, Vector> >> > (Integer.valueOf(0), tuple);
			 
			  });
			
			
			JavaPairRDD< Integer, ArrayList< Tuple2< Integer, Tuple2<WikiPage, Vector>> > > finalFix = fix.reduceByKey( 
			        (value1, value2) -> 
			        {
			           //eseguo l'unione dei due arraylist 
			            value2.forEach( (elem) -> 
			            {
			                value1.add(elem); 
			            });
			
			            return value1; 
			        }).cache();
			
			// In finalFix vi è quindi un unico elemento! 
			//System.out.println("Numero di elementi in finalFix:" + finalFix.count());
			
			
			JavaPairRDD<Integer , Tuple2<WikiPage, Vector>> newRDD = sc.parallelizePairs(finalFix.first()._2()); 
			
			JavaPairRDD< Integer , Iterable<Tuple2<WikiPage, Vector>> > groupedFinalClusters = newRDD.groupByKey(); 
			
			// stop monitoring for the ROUND3 execution time
		    long stopRoundThreeTime = System.currentTimeMillis();
		    long elapsedRoundThreeTime = stopRoundThreeTime - startRoundThreeTime;
		    // monitoring for the TOTAL execution time
		    long elapsedTime = stopRoundThreeTime - startRoundOneTime;
			  
			//*******************end Round 3***************************
	
		    double f_obj = Clustering_functions.objectiveFunction(groupedFinalClusters, clusters, sc );
		    // print data to result file
		    // format: Iteration,k,k_coeff,k_first,R1_time,R2_time,R3_time,f_obj,notes
		    output.println(iteration_count+","+k+","+k_coeff+","+k_first+","+elapsedRoundOneTime+","+elapsedRoundTwoTime+","+elapsedRoundThreeTime+","+f_obj+",,");
		    // Force the writing to the output file so if things go wrong we know everything up to the last iteration
	    	output.flush();
			iteration_count++;
		    
    	}
    	
    }
    }
    catch(Exception e)
    {
    	output.println("A fatal error occured");
    	// print the stacktrace in the output file
    	e.printStackTrace(output);
    }
    /*
	// Termine codice, nel seguito stampa diagnostica 
	
	   for(int f=0; f<k; f++)
	    {
	        List<Iterable<Tuple2<WikiPage, Vector>>> alist = groupedFinalClusters.lookup(Integer.valueOf(f));
	   
	        System.out.println("***********************************************************************************************"); 
	        System.out.println("****************************************************************"); 
	        System.out.println(); 
	        System.out.println("Numero cluster:" + f); 
	        System.out.println(); 
	
	        alist.forEach((elem1) ->
	        {
	           elem1.forEach((elem) ->
	        {
	           System.out.println("Titolo wikipage : " + elem._1().getTitle()); 
	      
	        });
	
	        });
	    } 
	
	
	    System.out.println("*********************************************************************************************************"); 
	    System.out.println("*********************************************************************************************************"); 
	    System.out.println(); 
	
	    System.out.println("Valore associato alla funzione obbiettivo per il k-center: "); 
	    System.out.println("[" + Clustering_functions.objectiveFunction(groupedFinalClusters, clusters, sc ) + "]");
	    System.out.println();
	    
	    // monitoring time strings
	    System.out.println("*********************************************************************************************************"); 
	    System.out.println();
	    System.out.println("DocToVec executed in " + TimeUnit.MILLISECONDS.toSeconds(elapsedDocToVecTime) + " seconds");
	    System.out.println("*********************************************************************************************************"); 
	    System.out.println();
	    System.out.println("Round 1 executed in " + TimeUnit.MILLISECONDS.toSeconds(elapsedRoundOneTime) + " seconds");
	    System.out.println("Round 2 executed in " + TimeUnit.MILLISECONDS.toSeconds(elapsedRoundTwoTime) + " seconds");
	    System.out.println("Round 3 executed in " + TimeUnit.MILLISECONDS.toSeconds(elapsedRoundThreeTime) + " seconds");
	    System.out.println("*********************************************************************************************************"); 
	    System.out.println();
	    System.out.println("Total execution time in " + TimeUnit.MILLISECONDS.toMinutes(elapsedTime) + " minutes");
*/
  }//[m]end main  


// l'algoritmo deve restituire una tupla, per ciascun documento devo calcolare k centri 
// @input = subset Pj 
// @output = Tuple2< Integer, ArrayList< Tuple2<WikiPage, Vector> > > 
// Aggiunta di k come parametro in ingresso al metodo 

public static Tuple2< Integer, ArrayList< Tuple2<WikiPage, Vector> > > farthestFirstTraversal(
    Tuple2< Integer, ArrayList< Tuple2<WikiPage, Vector> > > inputSubset , int k_param ) 
{
        //inputSubset = subset of elements for which we have to calculate centers 

        // K-center clustering 
        Integer subsetID = inputSubset._1(); 

        //subsetElements contiene i punti del subset Pj 
        ArrayList< Tuple2<WikiPage, Vector> > subsetElements = inputSubset._2(); 

        // number of points of the subset Pj that I consider 
        int numberOfTuples = subsetElements.size(); 

        //ArrayList in cui salvo i centri 
        ArrayList< Tuple2<WikiPage,Vector> > centers = new ArrayList< Tuple2<WikiPage,Vector> >();
         
        // number of clusters for each subset Pj 
        final int K = k_param;//<-----------------------------------------------------
        
        //Select a random element as the first center
	 // int index = randVal(0, numberOfTuples-1); 
	  //Tuple2<WikiPage,Vector> center = subsetElements.get(index); 
        
        //Select the first element as the first center
        int index = 0; 
  	    Tuple2<WikiPage,Vector> center = subsetElements.get(index);

	  //aggiungo il punto a centers 
	  centers.add(center); 
	  //rimuovo il centro da subsetElements 
	  subsetElements.remove(index);
           
        while(centers.size() != K) 
        {   
            
            ArrayList<Tuple2<Tuple2<WikiPage, Vector>, Double >> finalDistances = 
            new ArrayList<Tuple2<Tuple2<WikiPage, Vector>, Double >>(); 

            subsetElements.forEach( (point) ->
            {   
                    //ArrayList<Double> tempDistances = new ArrayList<Double>();
                    double minDistance = Distance.cosineDistance(point._2() , centers.get(0)._2() );

                    for(int r=1; r<centers.size(); r++)
                    {
                        double dist = Distance.cosineDistance(point._2(), centers.get(r)._2() ); 
                        if(dist < minDistance) 
                        {
                           minDistance = dist; 
                        }
                    }
                    // minDistance è già il minimo delle distanze  
                    finalDistances.add(new Tuple2<Tuple2<WikiPage, Vector>, Double >(point, minDistance)); 
                    
            } ); 

                //prendo il massimo delle distanze contenute in finalDistances 
                Tuple2< Tuple2<WikiPage, Vector> , Double > newTuple = finalDistances.get(0); 
                Tuple2< Tuple2<WikiPage, Vector> , Double > max = newTuple; 

                for(int r=1; r<finalDistances.size(); r++)
                {
                    if(finalDistances.get(r)._2() > max._2())
                    {
                        max = finalDistances.get(r); 
                    }
                }


                // max contiene ora il centro desiderato 
                // aggiungo max a centers e rimuovo max da subsetElements 
                centers.add(max._1()); 
                subsetElements.remove(max._1()); 
            
        } 


return new Tuple2< Integer, ArrayList< Tuple2<WikiPage, Vector> > >(subsetID, centers); 
   

 }//end[m]farthest_First_Traversal 



//restituisce un intero compreso tra minVal e maxVal 
public static int randVal(int minVal, int maxVal)
    { 
        int diff = maxVal-minVal;
        return (int)(Math.random()*((double)diff+1.0d))+minVal;
    }



// **********************************Metodo Partition(Pj,S)****************************************************
public static ArrayList< Tuple2< Integer , Tuple2<WikiPage, Vector> >> partition(Tuple2< Integer, ArrayList< Tuple2<WikiPage, Vector> > > pJ,
  ArrayList< Tuple2 < Integer, Tuple2<WikiPage, Vector > > > s) 
{
  
  ArrayList< Tuple2< Integer , Tuple2<WikiPage, Vector> >> clusterIndexPlusElement = 
  new ArrayList< Tuple2< Integer , Tuple2<WikiPage, Vector> >>(); 

  pJ._2().forEach( (tuple) ->
    {
       ArrayList<Tuple2<Integer, Double>> distances = new ArrayList<Tuple2<Integer, Double>>(); 
        s.forEach( (elem) -> 
            {
                double dist = Distance.cosineDistance(elem._2()._2() , tuple._2());
                distances.add(new Tuple2<Integer, Double>(elem._1(), dist)); 
            
            });

        
          //calcolo la minima distanza, ovvero il minimo dell'array distances 
         Tuple2<Integer, Double> min = distances.get(0); 
           
            for(int g=1; g<distances.size(); g++)
            {
              if( distances.get(g)._2().doubleValue() < min._2().doubleValue() )
                 min = distances.get(g); 
            }
        // min._1() = indice del cluster 
        // tuple = elemento che deve andare in quel cluster 
        
        clusterIndexPlusElement.add(new Tuple2< Integer , Tuple2<WikiPage, Vector> >(min._1(), tuple) );
    
    });


   
return clusterIndexPlusElement; 


}



}//[c]end class 
