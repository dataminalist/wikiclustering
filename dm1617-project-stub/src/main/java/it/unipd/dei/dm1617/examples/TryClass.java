package it.unipd.dei.dm1617.examples;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.linalg.Vector;

import it.unipd.dei.dm1617.WikiPage;
import scala.Tuple2;

public class TryClass {

	public static void main(String[] args) {
		System.setProperty("hadoop.home.dir", "c:\\winutil");
		
		SparkConf conf = new SparkConf(true).setAppName("k-Center_Map_Reduce");
	    JavaSparkContext sc = new JavaSparkContext(conf);
	    
	    JavaPairRDD<Integer , Tuple2<WikiPage, Vector>> newRDD = JavaPairRDD.fromJavaRDD(sc.objectFile("./clusterized-medium"));
		
	    Clustering_functions.classFrequency(newRDD,100,2);

	}

}
